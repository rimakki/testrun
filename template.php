<?php

/**
 * @file
 * This file provides theme override functions for the testrun theme.
 */

/**
 * Implements hook_template_preprocess_html().
 *
 * Preprocess variables for html.tpl.php.
 */
function testrun_preprocess_html(&$variables) {

  // Add default apple touch icon

  $apple_icon =  array(
    '#tag' => 'link',
    '#attributes' => array(
      'href' => file_create_url(drupal_get_path('theme', 'testrun') . '/icons/apple-touch-icon.png'),
      'rel' => 'apple-touch-icon-precomposed',
    ),
  );

  drupal_add_html_head($apple_icon, 'apple-touch-icon');

  // Add default precomposed apple touch icon

  $apple_icon =  array(
    '#tag' => 'link',
    '#attributes' => array(
      'href' => file_create_url(drupal_get_path('theme', 'testrun') . '/icons/apple-touch-icon-precomposed.png'),
      'rel' => 'apple-touch-icon-precomposed',
    ),
  );

  drupal_add_html_head($apple_icon, 'apple-touch-icon-precomposed');

  // Adding all apple icon sizes
  $apple_icon_sizes = array(57,60,72,76,114,120,144,152,180);

  foreach ($apple_icon_sizes as $size) {
    $apple = array(
      '#tag' => 'link',
      '#attributes' => array(
        'href' => file_create_url(drupal_get_path('theme', 'testrun') . '/icons/apple-touch-icon-' . $size . 'x' . $size . '.png'),
        'rel' => 'apple-touch-icon-precomposed',
        'sizes' => $size . 'x' . $size,
      ),
    );
    drupal_add_html_head($apple, 'apple-touch-icon-'.$size);
  }


  // Add viewport metatag for mobile devices.
  $meta_viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1.0',
    ),
  );
  drupal_add_html_head($meta_viewport, 'meta_viewport');

  // Set up IE meta tag to force IE rendering mode.
  $meta_ie_render_engine = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'http-equiv' => 'X-UA-Compatible',
      'content' =>  'IE=edge,chrome=1',
    ),
  );
  drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');

  // Add Google Fonts.
  drupal_add_css('//fonts.googleapis.com/css?family=Arimo:400,400italic,700', array('group' => CSS_THEME, 'type' => 'external'));

  // Additional classes for body element.
  if (user_access('administer site configuration')) {
    $variables['classes_array'][] = 'admin';
  }

  // Add classes for website section.
  if (!$variables['is_front']) {
    $path = drupal_get_path_alias($_GET['q']);
    if (arg(0) == 'node' && arg(1) == 'add') {
      $variables['classes_array'][] = 'section-node-add';
    }
    elseif (arg(0)== 'node' && is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
      $variables['classes_array'][] = 'section-node-' . arg(2); // Add 'section-node-edit' or 'section-node-delete'
    }
    else {
      list($section, ) = explode('/', $path, 2);
      $variables['classes_array'][] = testrun_id_safe('section-' . $section);
    }
  }
}

/**
 * Implements hook_template_preprocess_node().
 *
 * Preprocess variables for node.tpl.php.
 */
function testrun_preprocess_node(&$variables) {
  global $user;
  if ($variables['node']->uid && $variables['node']->uid == $user->uid) {
    $variables['classes_array'][] = 'node-mine';
  }
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 *   - Ensure an ID starts with an alpha character by optionally adding an 'n'.
 *   - Replaces any character except A-Z, numbers, and underscores with dashes.
 *   - Converts entire string to lowercase.
 *
 * @param $string
 *   The string.
 * @return
 *   The converted string.
 */
function testrun_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = drupal_strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
    $string = 'id' . $string;
  }
  return $string;
}

/**
 * Implements hook_template_preprocess_entity().
 */
function testrun_preprocess_entity(&$variables) {
  if ($variables['entity_type'] == 'bean' && !empty($variables['elements']['#bundle']) && $variables['elements']['#bundle'] == 'anywhere_button') {
    $imagevars = array('path' => $variables['field_button_image'][0]['uri']);
    if (!empty($variables['field_button_target'][0]['title'])) {
      $imagevars['alt'] = $variables['field_button_target'][0]['title'];
      $imagevars['title'] = $variables['field_button_target'][0]['title'];
    }
    $image = theme('image', $imagevars);
    $variables['content']['formatted_button'] = array(
       '#markup' =>l($image, $variables['field_button_target'][0]['url'], array('html' => TRUE)),
    );
    unset($variables['content']['field_button_target']);
    unset($variables['content']['field_button_image']);
  }
}

/**
 * Implements hook_template_preprocess_field().
 */
function testrun_preprocess_field(&$variables, $hook) {

  // Specify alternate templates for selected fields.

  switch ($variables['element']['#field_name']) {
    case 'field_program_xfer_schools':
      $variables['theme_hook_suggestions'][] = 'field__list';
      $variables['field_list_type'] = 'ul';
      break;
  }
}

/**
 * Implements hook_template_preprocess_username().
 *
 * @see realname_update()
 */
function testrun_preprocess_username(&$variables) {

  if ($pattern = theme_get_setting('testrun_realname_pattern')) {
    // $account = $variables['account'];
    $account = user_load($variables['account']->uid);
    // dpm($account, 'account');

    // Perform token replacement on the real name pattern.
    $realname = token_replace($pattern, array('user' => $account), array('clear' => TRUE, 'sanitize' => FALSE));

    // Remove any HTML tags.
    $realname = strip_tags(decode_entities($realname));

    // Remove double spaces (if a token had no value).
    $realname = preg_replace('/ {2,}/', ' ', $realname);

    // Trim the name to 255 characters.
    $realname = truncate_utf8(trim($realname), 255);

    // Substitute the real name if it is not empty, otherwise, leave the value unchanged.
    $variables['name'] = !empty($realname) ? $realname : $variables['name'];
  }
}

/**
 * Implements hook_form_alter()
 */
function testrun_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {

    $form['search_block_form']['#title_display'] = 'invisible';
    $form['search_block_form']['#size'] = 13;

    if (module_exists('google_cse')) {
        // Suppress Google CSE javascript.
        if (!empty($form['#attributes']['class'])) {
            foreach ($form['#attributes']['class'] as $key => $value){
              if ($value == 'google-cse') {
                unset($form['#attributes']['class'][$key]);
              }
            }
            if (empty($form['#attributes']['class'])) {
              unset($form['#attributes']['class']);
            }
        }

        // Suppress Google CSE field title.
        unset($form['search_block_form']['#attributes']['title']);

        // Suppress Google CSE default search value.
        $form['search_block_form']['#default_value'] = NULL;
    }
  }
}

/**
 * Implements hook_page_alter().
 */
function testrun_page_alter(&$page) {
  if (!empty($page['content']['system_main']['nodes'])) {
    $nodes = &$page['content']['system_main']['nodes'];
    $nid = key($nodes);
    if (!empty($nodes[$nid]['field_leftnav'])) {
      $page['sidebar_first']['field_leftnav'] = $nodes[$nid]['field_leftnav'];
      $page['sidebar_first']['field_leftnav']['#weight'] = '-99';
      $page['sidebar_first']['#sorted'] = FALSE;
      if (empty($page['sidebar_first']['#region'])) {
        $page['sidebar_first']['#region'] = 'sidebar_first';
        $page['sidebar_first']['#theme_wrappers'] = array('region');
      }
      unset($nodes[$nid]['field_leftnav']);
    }
  }
}

